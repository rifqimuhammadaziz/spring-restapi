package com.rifqimuhammadaziz.springrestapi.controller;

import com.rifqimuhammadaziz.springrestapi.model.request.UserDTO;
import com.rifqimuhammadaziz.springrestapi.model.response.BaseResponse;
import com.rifqimuhammadaziz.springrestapi.service.contract.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    @PostMapping("/save")
    public ResponseEntity<BaseResponse> saveUser(@RequestBody UserDTO userDTO) {
        return new ResponseEntity<>(userService.saveUser(userDTO), HttpStatus.OK);
    }
}
