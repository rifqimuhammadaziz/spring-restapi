package com.rifqimuhammadaziz.springrestapi.controller;


import com.rifqimuhammadaziz.springrestapi.model.request.CommentDTO;
import com.rifqimuhammadaziz.springrestapi.model.response.CommentProjection;
import com.rifqimuhammadaziz.springrestapi.model.response.CommentResponseDTO;
import com.rifqimuhammadaziz.springrestapi.repository.CommentRepository;
import com.rifqimuhammadaziz.springrestapi.service.contract.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/comments")
public class CommentController {

    private final CommentService commentService;
    private final CommentRepository commentRepository;

    @GetMapping()
    public ResponseEntity<List<CommentResponseDTO>> findAllComment() {
        return new ResponseEntity<>(commentService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<CommentResponseDTO>> findByID(@PathVariable("id") Long id) {
        return new ResponseEntity<>(commentService.findById(id), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<CommentResponseDTO> createComment(@RequestBody CommentDTO dto) {
        return new ResponseEntity<>(commentService.save(dto), HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<CommentResponseDTO> update(@PathVariable("id") Long id, @RequestBody CommentDTO dto) {
        return new ResponseEntity<>(commentService.update(dto, id), HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> delete(@RequestParam("id") Long id) {
        return new ResponseEntity<>(commentService.delete(id), HttpStatus.OK);
    }

    @GetMapping("/findAllWithProjection")
    public ResponseEntity<List<CommentProjection>> findAllWithProjection() {
        return new ResponseEntity<>(commentRepository.findAllWithProjection(), HttpStatus.ACCEPTED);
    }
}
