package com.rifqimuhammadaziz.springrestapi.service.implementation;

import com.rifqimuhammadaziz.springrestapi.entity.User;
import com.rifqimuhammadaziz.springrestapi.model.request.UserDTO;
import com.rifqimuhammadaziz.springrestapi.model.response.BaseResponse;
import com.rifqimuhammadaziz.springrestapi.repository.UserRepository;
import com.rifqimuhammadaziz.springrestapi.service.contract.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public BaseResponse saveUser(UserDTO userDTO) {
        BaseResponse response = new BaseResponse();

        BaseResponse validateResult = validateUser(userDTO);
        if (validateResult.isSuccess()) {
            User user = toEntity(userDTO);
            User saveUser = userRepository.save(user);
            response.setData(saveUser);
            response.setSuccess(true);
            response.setCode(String.valueOf(HttpStatus.OK));
        } else {
            response.setSuccess(false);
            response.setCode(String.valueOf(HttpStatus.BAD_REQUEST));
            return response;
        }

        return response;
    }

    private BaseResponse validateUser(UserDTO userDTO) {
        BaseResponse response = new BaseResponse();
        response.setSuccess(true);

        List<User> user = userRepository.findByEmail(userDTO.getEmail());
        if (!ObjectUtils.isEmpty(user)) {
            response.setSuccess(false);
            response.setCode(String.valueOf(HttpStatus.BAD_GATEWAY));
            return response;
        }

        return response;
    }

    private User toEntity(UserDTO userDTO) {
        User user = new User();
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setUsername(userDTO.getUsername());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setPhoneNumber(userDTO.getPhoneNumber());
        user.setStartTime(userDTO.getStartTime());
        user.setEndTime(userDTO.getEndTime());
        user.setCreatedDate(new Date());
        user.setLastModifiedDate(new Date());
        return user;
    }
}
