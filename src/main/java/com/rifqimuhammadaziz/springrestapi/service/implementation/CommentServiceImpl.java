package com.rifqimuhammadaziz.springrestapi.service.implementation;

import com.rifqimuhammadaziz.springrestapi.entity.Comment;
import com.rifqimuhammadaziz.springrestapi.entity.Post;
import com.rifqimuhammadaziz.springrestapi.exception.ResourceNotFoundException;
import com.rifqimuhammadaziz.springrestapi.repository.CommentRepository;
import com.rifqimuhammadaziz.springrestapi.model.request.CommentDTO;
import com.rifqimuhammadaziz.springrestapi.model.response.CommentResponseDTO;
import com.rifqimuhammadaziz.springrestapi.repository.PostRepository;
import com.rifqimuhammadaziz.springrestapi.service.contract.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final PostRepository postRepository;

    @Override
    public List<CommentResponseDTO> findAll() {
        List<Comment> comments = commentRepository.findAll();
        return comments.stream().map(comment -> mapperToCommentDTO(comment)).collect(Collectors.toList());
    }

    @Override
    public Optional<CommentResponseDTO> findById(Long id) {
        commentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Comment not found"));
        Comment comment = commentRepository.findById(id).get();
        return Optional.of(mapperToCommentDTO(comment));
    }

    @Override
    public CommentResponseDTO save(CommentDTO comment) {
        Post post = postRepository.findById(comment.getPostId())
                .orElseThrow(() -> new ResourceNotFoundException("Post not found with id: " + comment.getPostId()));
        Comment saveComment = new Comment();
        saveComment.setName(comment.getName());
        saveComment.setEmail(comment.getEmail());
        saveComment.setBody(comment.getBody());
        saveComment.setPost(post);
        return mapperToCommentDTO(commentRepository.save(saveComment));
    }

    @Override
    public CommentResponseDTO update(CommentDTO dto, Long id) {
        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Comment not found with id: " + id));
//        Post post = postRepository.findById(dto.getPostId())
//                        .orElseThrow(() -> new ResourceNotFoundException("Post not found with id: " + dto.getPostId()));

        comment.setEmail(dto.getEmail());
        comment.setBody(dto.getBody());
        comment.setName(dto.getName());
//        comment.setPost(post);
        return mapperToCommentDTO(commentRepository.save(comment));
    }

    @Override
    public String delete(Long id) {
        commentRepository.deleteById(id);
        return "Success delete";
    }

    public static CommentResponseDTO mapperToCommentDTO(Comment entity) {
        CommentResponseDTO dto = new CommentResponseDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setEmail(entity.getEmail());
        dto.setBody(entity.getBody());
        dto.setPost(entity.getPost());
        return dto;
    }
}
