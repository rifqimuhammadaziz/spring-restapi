package com.rifqimuhammadaziz.springrestapi.service.implementation;

import com.rifqimuhammadaziz.springrestapi.entity.Post;
import com.rifqimuhammadaziz.springrestapi.exception.ResourceNotFoundException;
import com.rifqimuhammadaziz.springrestapi.repository.PostRepository;
import com.rifqimuhammadaziz.springrestapi.model.request.PostDTO;
import com.rifqimuhammadaziz.springrestapi.model.response.PostResponseDTO;
import com.rifqimuhammadaziz.springrestapi.service.contract.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.rifqimuhammadaziz.springrestapi.service.implementation.CommentServiceImpl.mapperToCommentDTO;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;

    @Override
    public List<PostResponseDTO> findAll() {
        List<Post> posts = postRepository.findAll();
        return posts.stream().map(post -> mapperToDTO(post)).collect(Collectors.toList());
    }

    @Override
    public Optional<PostResponseDTO> findById(Long id) {
        postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Post not found with id: " +id));
        return Optional.of(mapperToDTO(postRepository.findById(id).get()));
    }

    @Override
    public PostResponseDTO save(PostDTO dto) {
        Post post = new Post();
        post.setTitle(dto.getTitle());
        post.setDescription(dto.getDescription());
        post.setContent(dto.getContent());

        Post savePost = postRepository.save(post);
        return mapperToDTO(savePost);
    }

    @Override
    public PostResponseDTO update(PostDTO dto, Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Post not found with id: " + id));
        post.setTitle(dto.getTitle());
        post.setDescription(dto.getDescription());
        post.setContent(dto.getContent());
        return mapperToDTO(post);
    }

    @Override
    public String delete(Long id) {
        postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Post not found"));
        postRepository.deleteById(id);
        return "Post deleted successfully";
    }

    private PostResponseDTO mapperToDTO(Post entity) {
        PostResponseDTO dto = new PostResponseDTO();
        dto.setId(entity.getId());
        dto.setTitle(entity.getTitle());
        dto.setDescription(entity.getDescription());
        dto.setContent(entity.getContent());
        if (entity.getComments() != null) {
            dto.setComments(entity.getComments().stream().map(comment -> mapperToCommentDTO(comment)).collect(Collectors.toSet()));
        }
        return dto;
    }
}
