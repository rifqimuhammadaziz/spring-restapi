package com.rifqimuhammadaziz.springrestapi.service.contract;

import com.rifqimuhammadaziz.springrestapi.model.request.UserDTO;
import com.rifqimuhammadaziz.springrestapi.model.response.BaseResponse;

public interface UserService {
    BaseResponse saveUser(UserDTO userDTO);
}
