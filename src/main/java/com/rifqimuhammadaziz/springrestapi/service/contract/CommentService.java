package com.rifqimuhammadaziz.springrestapi.service.contract;

import com.rifqimuhammadaziz.springrestapi.model.request.CommentDTO;
import com.rifqimuhammadaziz.springrestapi.model.response.CommentResponseDTO;

import java.util.List;
import java.util.Optional;

public interface CommentService {
    List<CommentResponseDTO> findAll();
    Optional<CommentResponseDTO> findById(Long id);
    CommentResponseDTO save(CommentDTO comment);
    CommentResponseDTO update(CommentDTO dto, Long id);
    String delete(Long id);
}
