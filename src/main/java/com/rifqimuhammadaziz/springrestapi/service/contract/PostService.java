package com.rifqimuhammadaziz.springrestapi.service.contract;

import com.rifqimuhammadaziz.springrestapi.model.request.PostDTO;
import com.rifqimuhammadaziz.springrestapi.model.response.PostResponseDTO;

import java.util.List;
import java.util.Optional;

public interface PostService {
    List<PostResponseDTO> findAll();
    Optional<PostResponseDTO> findById(Long id);
    PostResponseDTO save(PostDTO dto);
    PostResponseDTO update(PostDTO dto, Long id);
    String delete(Long id);
}
