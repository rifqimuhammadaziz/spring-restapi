package com.rifqimuhammadaziz.springrestapi.entity;

import lombok.Data;

import javax.persistence.*;

// Custom QUERY used to repository
@NamedQueries({
        @NamedQuery(name = "Comment.findAllComments", query = "SELECT c FROM Comment c"),
        @NamedQuery(name = "Comment.findByCommentId", query = "SELECT c FROM Comment c WHERE c.id = ?1"),
        @NamedQuery(name = "Comment.findWithLike", query = "SELECT c FROM Comment c WHERE c.name LIKE ?1")
})

// SQL Result for native query
@SqlResultSetMappings({
        @SqlResultSetMapping(name = "findAllWithNativeQuery", entities = {@EntityResult(entityClass = Comment.class)}),
        @SqlResultSetMapping(name = "findByIdWithNativeQuery", entities = {@EntityResult(entityClass = Comment.class)})
})

@NamedNativeQueries({
        @NamedNativeQuery(
                name = "Comment.findAllWithNativeQuery",
                query = "SELECT c.* FROM comments",
                resultSetMapping = "findAllWithNativeQuery"),
        @NamedNativeQuery(
                name = "Comment.findByCommentIdWithNativeQuery",
                query = "SELECT c.* FROM comments c WHERE c.id = ?1",
                resultSetMapping = "findByIdWithNativeQuery"
        )
})
@Data
@Entity
@Table(name = "comments", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "body")
    private String body;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id")
    private Post post;
}
