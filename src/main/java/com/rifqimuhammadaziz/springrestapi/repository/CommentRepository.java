package com.rifqimuhammadaziz.springrestapi.repository;

import com.rifqimuhammadaziz.springrestapi.entity.Comment;
import com.rifqimuhammadaziz.springrestapi.model.response.CommentProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    // Named Query
    List<Comment> findAllComments();
    Comment findByCommentId(Long id);
    List<Comment> findWithLike(String name);

    // Named Native Query
    List<Comment> findAllWithNativeQuery();
    Comment findByCommentIdWithNativeQuery(Long id);

    // JPA Query
    @Query(value = "SELECT c FROM Comment c WHERE c.id = ?1")
    Comment findByIdJPA(Long id);

    // JPA Projection
    @Query(value = "SELECT c.comment_id as commentId, c.name as commentName, c.email as commentEmail, c.body as commentBody" +
            " FROM comments c", nativeQuery = true)
    List<CommentProjection> findAllWithProjection();


}
