package com.rifqimuhammadaziz.springrestapi.repository.custom;

import com.rifqimuhammadaziz.springrestapi.model.response.PageDTO;

public interface PostRepositoryCustom {
    PageDTO findAllWithCustomPage(int size, int page, String direction, String properties, String content, String title);

}
