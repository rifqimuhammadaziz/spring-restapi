package com.rifqimuhammadaziz.springrestapi.model.response;

public interface CommentProjection {
    Long getCommentId();
    String getCommentName();
    String getCommentEmail();
    String getCommentBody();
}
