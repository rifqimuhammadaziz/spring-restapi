package com.rifqimuhammadaziz.springrestapi.model.request;

import com.rifqimuhammadaziz.springrestapi.entity.Post;
import lombok.Data;

@Data
public class CommentDTO {
    private String name;
    private String email;
    private String body;
    private Long postId;
}
