package com.rifqimuhammadaziz.springrestapi.model.request;

import com.rifqimuhammadaziz.springrestapi.entity.Comment;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
public class PostDTO {

//    @NotNull(message = "Title is required")
//    @NotEmpty(message = "Title can not empty, please fill title")
//    @Length(min = 5, max = 100, message = "Invalid length of title, please fill with 5-100 characters")
    private String title;

//    @NotNull(message = "Description is required")
//    @NotEmpty(message = "Description can not empty, please fill description")
//    @Length(min = 5, max = 100, message = "Invalid length of description, please fill with 5-100 characters")
    private String description;

    private Integer maximumOfComments;

//    @NotNull(message = "Content is required")
//    @NotEmpty(message = "Content can not empty, please fill content")
//    @Length(min = 5, max = 100, message = "Invalid length of content, please fill with 5-100 characters")
    private String content;

    private String phoneNumber;
}
